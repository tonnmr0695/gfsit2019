﻿using System.ComponentModel;
using System.Drawing;
using System.Windows.Forms;

public class LollipopProgressBar : Control
{
    #region variables

    private int ProgressNum = 10;
    private string ProgressColor = "#BD1D31";

    private Color BackGroundColor;

    #endregion variables

    #region Properties

    [Category("Appearance")]
    public string BGColor
    {
        get { return ProgressColor; }
        set
        {
            ProgressColor = value;
            Invalidate();
        }
    }

    [Category("Behavior")]
    public int Value
    {
        get { return ProgressNum; }
        set
        {
            ProgressNum = value;
            Invalidate();
        }
    }

    #endregion Properties

    public LollipopProgressBar()
    {
        Width = 300; Height = 4; DoubleBuffered = true;
    }

    protected override void OnPaint(PaintEventArgs e)
    {
        base.OnPaint(e);

        Graphics G = e.Graphics;
        G.Clear(Parent.BackColor);

        BackGroundColor = ColorTranslator.FromHtml(ProgressColor);
        G.FillRectangle(new SolidBrush(Color.FromArgb(68, BackGroundColor)), 0, 0, Width, Height);

        if (ProgressNum <= 101 && ProgressNum >= 0)
        { G.FillRectangle(new SolidBrush(BackGroundColor), 0, 0, (Width * ProgressNum) / (100 - 0), Height); }
        else
        {
            ProgressNum = 10;

            MessageBox.Show("Wrong value...!", "Lollipop Theme", MessageBoxButtons.OK, MessageBoxIcon.Information);
            G.FillRectangle(new SolidBrush(BackGroundColor), 0, 0, (Width * ProgressNum) / (100 - 0), Height);
        }
    }
}