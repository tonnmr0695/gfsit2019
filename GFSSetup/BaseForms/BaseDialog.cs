﻿using System;
using System.Windows.Forms;

namespace GFSSetup
{
    public partial class BaseDialog : Form
    {
        #region FormCustomizations

        private const int CS_DROPSHADOW = 0x00020000;

        protected override CreateParams CreateParams
        {
            get
            {
                // add the drop shadow flag for automatically drawing
                // a drop shadow around the form
                CreateParams cp = base.CreateParams;
                cp.ClassStyle |= CS_DROPSHADOW;
                return cp;
            }
        }

        protected override void OnLoad(EventArgs e)
        {
            this.AutoScaleDimensions = new System.Drawing.SizeF(96F, 96F);

            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Dpi;
            base.OnLoad(e);
        }

        #endregion FormCustomizations

        public BaseDialog()
        {
            InitializeComponent();
        }

        private void TitleBar_MouseDown(object sender, MouseEventArgs e)
        {
            if (e.Button == MouseButtons.Left)
            {
                // Release the mouse capture started by the mouse down.
                TitleBar.Capture = false;

                // Create and send a WM_NCLBUTTONDOWN message.
                const int WM_NCLBUTTONDOWN = 0x00A1;
                const int HTCAPTION = 2;
                Message msg =
                    Message.Create(this.Handle, WM_NCLBUTTONDOWN,
                        new IntPtr(HTCAPTION), IntPtr.Zero);
                this.DefWndProc(ref msg);
            }
        }

        private void FormClose_Click(object sender, EventArgs e)
        {
            Close();
        }
    }
}