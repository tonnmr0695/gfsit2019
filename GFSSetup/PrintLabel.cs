﻿using System;
using System.IO;
using System.Runtime.InteropServices;
using System.Text;
using System.Windows.Forms;

namespace GFSSetup
{
    public partial class PrintLabel : BaseDialog
    {
        public PrintLabel()
        {
            InitializeComponent();
        }

        private void lollipopButton1_Click(object sender, EventArgs e)
        {
            string s =
                "^XA^FO 0,10^FO0,25^FB632,1,0,C,0^ASN,25,25" +
                "^FO90,50 ^FD" + Environment.MachineName.ToString().ToUpper() + "^FS" +
                "^FO0,0^GFA,43264,43264,00104,:Z64:eJzs3EFv20YWB/AhJhjugRF7pGHWvO5xBAEJgwhWL/0e9BqQL9ldCQESL9aIyAqwLq599SHIfoUce8sIBKSLkZ576IKCAOe2YGFgwyKCuG9IJWkXXfgNJarZdv6A5SSQ8nPEN8OnISeE6Ojo6Ojo6Ojo6Ojo6Ojo6Ojo6Ojo6Ojo6Ojo6Ojo6Ojo6Ojo6Ojo6Ojo6Oj8HpL48GCufsOfEeJtwWnmIen8oX7nABzfqd85jAXx/S04UY1Oh37zttfrZw9H4lAY2TIfj78V/9rfuDMYvsv93nzRuUjijC6Wy1n8j2Q52LCTZwdxfOFn80bb7Y1zs+H78/llL52Hm3bG8cj0s2sAfJGPCudlbzYWm3ZMcNzvxL50vNem7z8+MtKNO1AH4Hz+/Xjpd34UVDpPn9rpLN+487fCiV+DMzJKZ69XgzMD5+7X8sAYo0g69+75vdmGmZVjX0jnwTCewrf9fX/zdZD483h4Ac6e7/aHRb2BkyabrmsYp/EwN0269O0bI6cLf991vTQZbNxpz8O3pmm88c2YPDMyf891bT/Z9LzzGwvN/1fE/6Wj87uLF4XwKDoLeAzgyyJM5eWm2CfcCG9/4mC2lmMnA8Ipwkn6hVP8SEHxJ0qO25uHPfN2xxDNtRyHj6MU44Q7L2iyB47HjoNZKh7Zp8cqTjOMX9693aHEeGWnHXAGp4uDOBOPvMlC0eGfo5z5c/8AnNn0/HDkBlf+m3MFxzoIBd9BOeNLR4Ajpuyo0QiuekcqB8gcg9NEOVF4LB06YX+xWHAVMBXHnuGdUSadzoSdWKx5pubA8RH8AOdcXErnZMKeqTtwbASfYZyd6HlxfJIJe2qxXauCkye3Pg/GT7Sqgwk7rNERzejSl854wg7OG7tWr6vkOOMQNY8m/ej5sRw/c3CmbsPi10p1wOc4ZzCLLrJiPpiwIM4aljdRcbw0x50XvCgancn5rd1lML/dsexTpXGKPf/o6Oh88jGEGxAuf1W2iqoxBbOXiMVTI2mv5dgJOAOBccjKqTRLub0GuRPU7zh8n1giuN15+Sg4aYkTmpQt6ZEi5vAnxEE6y/uzpZnKlud08Veh6nTJZwTlHEzbj0cNB5z+9Pw4UXOgnYC3HeFcXh0O/RZ1LNnyvGG921/ys0BdgyJQDuHcLhwBp1RFB+qaBIgKWjm5t6jmOBwcevvzSqd5U9XhDiMC0VAYX5+B0xo6ZA3HQjt0LQdxLUU6Q94y3aLerqs4RsgRzsg6GPHWRTl+JlXqwAgRrwEnWPJmbmbFfKDseCk4mD4enBPOfxiW85uyA+MU5ejo6KyRjnz4igxSuZagHhindIG5yJ6HMCmcVnbgfEoXiD6RyCtU4ASkmgN9IpxQb3eKyXYNB/pEOAEFGCezR3kYUNqKzVuf/gvOE6RjLLxp4dyfN6o4XZYi+jeS0HN/Bu8bpe3HrroDfaKZYZylyXp9qLc59VsV7jGAum5YiD6RdODf0iwc3uqpO1DXFsN80qCTwoH3jbcSdUf2iQxVqN2PTqruyL4KszBIf+p8U9FB9Ik/cyrcOwOOxTB9YnhU1FtATd5C/Fy/5NzB9InjuFGOnwveqjROGbMCxBPzaOGN34SB8Y63lK7IlIE+kWaY/q1DMjucw2x6A122uiP7xFT3iTo6v/WY3sIW5Nk2nE5qyH61zBdbckRdjkuOj2n8/q83anR6Po224RwTKkyzkT4UHfqqQpeCdQaEZqZpw3EaDPPaHC/rJEYOjtvuzeJN32L3U8dLST4yGw3fn8UfDlQNDs2IN5XOAzGvz4GPD1AI23AS3zS24aS+GW3DkfPBtPY6gPGT0nx0UdZ1nY4cPzQvx2lU43wgzz/G23LeCetyPqRB6rk5fsuO/V+3Wtb1vm3L+ZBtHR+dXzHvP/sG1V5efD7FrI+u6cj10awT1u64vYbp9kXtjlwftRDrsISZ4xc7c7kcm7XESQXnicVQjj17tXsDjrG4nywrOF2k4zbnLx4MSUDP2/1Y2bECZjHEfZCE8Z3x8x1wTNNvqd/qAHVtMVRdc3njYEwCRnkFp1gfRdV150dwJqXz8UMVNg5nyONTON2qTrGeiFiPh/fN+OioKniHls6j+h1f1kFb1htvFffKqzoMc18NPXebpUPPeVN9/EAdmC7m+s/C/gEchwxmC85zZcdLmZlhrmdl5rh0koxz9fkNff4pU8/u0l/PQayobyS195qrJFtydD7RGOVUU1zJ40y9vLHzwWpdsnD8Cg62HzXKZYHCaVe4zoTtR41wPQfbjxrhJIVG9O/QlZJ7jO9bqWd9Fqg4T1DnOSOc3kAjmkNXSlqMD+5mnTNfyeninGhkNtr9kdufh+DMLtz+lYoD/ShF3aeanzLmt6j/YBw2T5vhayu59gXeKeoa55A93qLQlYbNSTO8skT3jwpOUdeo921iLFcOnzRz6TxScIrrtKg66NLr9063cP705xDvYPs3cEz20QktSxzW5LAPzr3S+bImp8FlvYGz1xRXVjL7Es+UDm4+MPd4E8aPdPjszOqPpwpOUQeY+S2f0CU0ovYMnAYfnFmdSMWR/egCsRveyJkx5xzmN3Dk/GZ55CsFR7Ef1dHR+ZTjieKbvBWpyoctU+yj9hkVN46u4djJALXPiKTlrvDdio7c947aZ7TafFDVcfg4xPUH1j898m/ycNg2X57sTpOTnUTJaYYC07/RkCw70ZI0h8/sdOl9my53lT4TSwezzwga+KQXN8CZOv3YdXfiPaWLMMW+d5wjxBEjQTzkLePSocZzpTULue8ds8+IGgshutKRy6OXLgVLxZH73jH7jKSTX6+c/NKOczVHHh/MPiNKrMLJ54Vj3ig6cp84Zp8RheoXbOUQh7wjjrqD2WdUOoZ4XDq7VRzM/aPCEskRM0JofVvRpbMbKb5vjmzIEE9M26IXn0sHxo/jPojV/j0On0eYfUYwX4vOeEnJLrXT3JFfSo6X5rj9P7YQML9JB+Y3x0xO1Bw4/yjsM6qwcFAlSv+zwhoxK9yoXCW0ys7gCjG2tQxbY/4DAAD//+2csYrbQBCGJRSkZq1LqeMEbq/U4UaQYOcF8g7qkiacSweO2EnA14ikdUjgyCPcG6wxOTWK06bLihRpdaRRccSZXa3L4BmFUzU/BjUyn7FHw+eRdvn1f7E+Cm1BH8j3GaAfeHeYdRLWRyd1Nw74KI5jfXSaduOAjy4dxHP1ex9VUTcO+OgSs9nq3kel+ObWz8ITOR78pFy14CE3zqPD51kfBTP6cdmcXw3r+dGvhsipEfdDrY8C53Ybv/z9KanehZSuCj5aNCivMj4KHFWKabU6XpSk5g0+uhUYTuujwJHycbbWHNIqE/DRErPOyPqo5mzuNGdXBorA0T6K4rQ+qjlF2XI8Sh1of0NxrCfCYSvM9yYcyt1aLMf6KFw/stxzKIUAHNTvY31U94My1hzAkTjROn+A6m/GR3V/20K9wfWTh5RlgeCjS0xdWx/V/bqAertS8zyg1AH4qIe5Tq2P6sPmJlt/lGPhUfqb9tEavaCUfbRT2Ec5fWVXm/leoLq93cxHA4nmDDuWnJmPDtXhE/12Xhl1fPTZzEdTxEJpy0k6tgQzH00QAxHf95KLE5kM3oixqCcgpQsSR89HMXMXzfkzlIl7KeaDZv62oe3EZOajGYbj+Web2ImBU72PVRHTdmIy81HMVwCckbuCv1vFMZilDEPaTkxmPnqO+Gi+u9ScoP6ilVcJ4k4yZj46Q7zFd/PRbuU4s68fdqV4ReWY+egUU29OaTji6TVwJjmRY+aJGZIDf+My8eJ6IURQ3h/ne8t5rjnOPXJmo9cr6Aenn0FF5TYkcsx8FMc520ROmqaqykVVxMQ60PNRzAMyvt71K4J+nYKKCugHNI6Zj9pZx0HOReQE6uGTsRDQ32gcMx89WmBO7etZy744WT8YV/XEkf1wOBwOh8PhcDgcDofD4XA4HA7nH/kLHeeMMA==:0ED9" +
                "^PQ1,0,1,Y^XZ";

            if (comboBox1.SelectedItem.ToString() == @"IT WEST B1 (\\gfsprint1\ITDEPTGX430t)")
            {
                try
                {
                    RawPrinterHelper.SendStringToPrinter(@"\\48664457-W10Y16\ZDesigner GX430t (Copy 1)", s);
                }
                catch
                {
                    MessageBox.Show("Error Printing", "Error Printing");
                }
            }

            if (comboBox1.SelectedItem.ToString() == @"IT EAST B3 (\\1W6MYW1-W10Y18\ZDesigner GX420t)")
            {
                try
                {
                    RawPrinterHelper.SendStringToPrinter(@"\\1W6MYW1-W10Y18\ZDesigner GX420t (Copy 1)", s);
                }
                catch
                {
                    MessageBox.Show("Error Printing", "Error Printing");
                }
            }

            if (comboBox1.SelectedItem.ToString() == ("TEST"))
            {
                try
                {
                    RawPrinterHelper.SendStringToPrinter(@"\\48664457-W10Y16\ZDesigner GX430t", s);
                }
                catch
                {
                    MessageBox.Show("Error Printing", "Error Printing");
                }
            }
        }

        public class RawPrinterHelper
        {
            // Structure and API declarions:
            [StructLayout(LayoutKind.Sequential, CharSet = CharSet.Ansi)]
            public class DOCINFOA
            {
                [MarshalAs(UnmanagedType.LPStr)] public string pDocName;
                [MarshalAs(UnmanagedType.LPStr)] public string pOutputFile;
                [MarshalAs(UnmanagedType.LPStr)] public string pDataType;
            }

            [DllImport("winspool.Drv", EntryPoint = "OpenPrinterA", SetLastError = true, CharSet = CharSet.Ansi, ExactSpelling = true, CallingConvention = CallingConvention.StdCall)]
            public static extern bool OpenPrinter([MarshalAs(UnmanagedType.LPStr)] string szPrinter, out IntPtr hPrinter, IntPtr pd);

            [DllImport("winspool.Drv", EntryPoint = "ClosePrinter", SetLastError = true, ExactSpelling = true, CallingConvention = CallingConvention.StdCall)]
            public static extern bool ClosePrinter(IntPtr hPrinter);

            [DllImport("winspool.Drv", EntryPoint = "StartDocPrinterA", SetLastError = true, CharSet = CharSet.Ansi, ExactSpelling = true, CallingConvention = CallingConvention.StdCall)]
            public static extern bool StartDocPrinter(IntPtr hPrinter, Int32 level, [In, MarshalAs(UnmanagedType.LPStruct)] DOCINFOA di);

            [DllImport("winspool.Drv", EntryPoint = "EndDocPrinter", SetLastError = true, ExactSpelling = true, CallingConvention = CallingConvention.StdCall)]
            public static extern bool EndDocPrinter(IntPtr hPrinter);

            [DllImport("winspool.Drv", EntryPoint = "StartPagePrinter", SetLastError = true, ExactSpelling = true, CallingConvention = CallingConvention.StdCall)]
            public static extern bool StartPagePrinter(IntPtr hPrinter);

            [DllImport("winspool.Drv", EntryPoint = "EndPagePrinter", SetLastError = true, ExactSpelling = true, CallingConvention = CallingConvention.StdCall)]
            public static extern bool EndPagePrinter(IntPtr hPrinter);

            [DllImport("winspool.Drv", EntryPoint = "WritePrinter", SetLastError = true, ExactSpelling = true, CallingConvention = CallingConvention.StdCall)]
            public static extern bool WritePrinter(IntPtr hPrinter, IntPtr pBytes, Int32 dwCount, out Int32 dwWritten);

            // SendBytesToPrinter()
            // When the function is given a printer name and an unmanaged array
            // of bytes, the function sends those bytes to the print queue.
            // Returns true on success, false on failure.
            public static bool SendBytesToPrinter(string szPrinterName, IntPtr pBytes, Int32 dwCount)
            {
                Int32 dwError = 0, dwWritten = 0;
                IntPtr hPrinter = new IntPtr(0);
                DOCINFOA di = new DOCINFOA();
                bool bSuccess = false; // Assume failure unless you specifically succeed.

                di.pDocName = "My C#.NET RAW Document";
                di.pDataType = "RAW";

                // Open the printer.
                if (OpenPrinter(szPrinterName.Normalize(), out hPrinter, IntPtr.Zero))
                {
                    // Start a document.
                    if (StartDocPrinter(hPrinter, 1, di))
                    {
                        // Start a page.
                        if (StartPagePrinter(hPrinter))
                        {
                            // Write your bytes.
                            bSuccess = WritePrinter(hPrinter, pBytes, dwCount, out dwWritten);
                            EndPagePrinter(hPrinter);
                        }
                        EndDocPrinter(hPrinter);
                    }
                    ClosePrinter(hPrinter);
                }
                // If you did not succeed, GetLastError may give more information
                // about why not.
                if (bSuccess == false)
                {
                    dwError = Marshal.GetLastWin32Error();
                }
                return bSuccess;
            }

            public static bool SendFileToPrinter(string szPrinterName, string szFileName)
            {
                // Open the file.
                FileStream fs = new FileStream(szFileName, FileMode.Open);
                // Create a BinaryReader on the file.
                BinaryReader br = new BinaryReader(fs);
                // Dim an array of bytes big enough to hold the file's contents.
                Byte[] bytes = new Byte[fs.Length];
                bool bSuccess = false;
                // Your unmanaged pointer.
                IntPtr pUnmanagedBytes = new IntPtr(0);
                int nLength;

                nLength = Convert.ToInt32(fs.Length);
                // Read the contents of the file into the array.
                bytes = br.ReadBytes(nLength);
                // Allocate some unmanaged memory for those bytes.
                pUnmanagedBytes = Marshal.AllocCoTaskMem(nLength);
                // Copy the managed byte array into the unmanaged array.
                Marshal.Copy(bytes, 0, pUnmanagedBytes, nLength);
                // Send the unmanaged bytes to the printer.
                bSuccess = SendBytesToPrinter(szPrinterName, pUnmanagedBytes, nLength);
                // Free the unmanaged memory that you allocated earlier.
                Marshal.FreeCoTaskMem(pUnmanagedBytes);
                return bSuccess;
            }

            public static bool SendStringToPrinter(string szPrinterName, string szString)
            {
                IntPtr pBytes;
                Int32 dwCount;
                // How many characters are in the string?
                dwCount = szString.Length;
                // Assume that the printer is expecting ANSI text, and then convert
                // the string to ANSI text.
                pBytes = Marshal.StringToCoTaskMemAnsi(szString);
                // Send the converted ANSI string to the printer.
                SendBytesToPrinter(szPrinterName, pBytes, dwCount);
                Marshal.FreeCoTaskMem(pBytes);
                return true;
            }

            public static bool SendTextFileToPrinter(string szFileName, string printerName)
            {
                var sb = new StringBuilder();

                using (var sr = new StreamReader(szFileName, Encoding.Default))
                {
                    while (!sr.EndOfStream)
                    {
                        sb.AppendLine(sr.ReadLine());
                    }
                }

                return RawPrinterHelper.SendStringToPrinter(printerName, sb.ToString());
            }
        }

        private void lollipopButton2_Click(object sender, EventArgs e)
        {
            Close();
        }
    }
}