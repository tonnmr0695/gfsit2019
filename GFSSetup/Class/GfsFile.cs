﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Windows.Forms;

namespace GFSSetup.Class
{
    public class GfsFile
    {
        public void Copy(string FilePath, string FileDestination)
        {
            try
            {
                File.Copy(FilePath, FileDestination, true);
            }
            catch (Exception e)
            {
                MessageBox.Show(e.Message.ToString() + ": " + FilePath, "Exception");
            }
        }

        public void CreateShortcut(string targetPath, string iconLocation, string shortcutName, string shortcutPath, string targetFileLocation)
        {
            try
            {
                string shortcutLocation = System.IO.Path.Combine(shortcutPath, shortcutName + ".lnk");
                IWshRuntimeLibrary.WshShell shell = new IWshRuntimeLibrary.WshShell();
                IWshRuntimeLibrary.IWshShortcut shortcut = (IWshRuntimeLibrary.IWshShortcut)shell.CreateShortcut(shortcutLocation);

                shortcut.TargetPath = targetPath;
                shortcut.IconLocation = iconLocation;
                shortcut.Save();
            }
            catch (Exception e)
            {
                MessageBox.Show(e.Message.ToString(), "Exception");
            }
        }

        public void GetFileType(string Directory, string FileType, string Switch)
        {
            DirectoryInfo DIRINF = new DirectoryInfo(Directory);
            List<FileInfo> FINFO = DIRINF.GetFiles("*." + FileType, SearchOption.AllDirectories).ToList();
            List<object> Data = new List<object>();
            foreach (FileInfo FoundFile in FINFO)
            {
                var Name = FoundFile.Name;
                var Path = FoundFile.FullName;
                var Extension = FoundFile.FullName;
                var Length = FoundFile.Length;

                var process = new GfsStartProcess();
                process.Start(Path, Switch);
            }
        }

        public void LogCSV(string Path, string ComputerName, string DateTime)
        {
            try
            {
                File.AppendAllText(Path, ComputerName + "," + DateTime + Environment.NewLine);
            }
            catch
            {
            }
        }
    }
}