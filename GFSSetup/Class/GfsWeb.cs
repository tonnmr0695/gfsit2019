﻿using System;
using System.Net;
using System.Windows.Forms;

namespace GFSSetup.Class
{
    public class GfsWeb
    {
        public void Request(string WebURL, string Destination)
        {
            //disabled certificate verification, site is selfsigned and not considered safe in the browser.
            System.Net.ServicePointManager.ServerCertificateValidationCallback = delegate { return true; };

            WebClient Client = new WebClient();
            try
            {
                Client.DownloadFile(WebURL, Destination);
            }
            catch (Exception e)
            {
                MessageBox.Show(e.Message.ToString(), "Exception");
            }

            Client.Dispose();
        }
    }
}