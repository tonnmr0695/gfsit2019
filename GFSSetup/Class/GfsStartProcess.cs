﻿using System;
using System.Diagnostics;
using System.Windows.Forms;

namespace GFSSetup.Class
{
    public class GfsStartProcess
    {
        public void Start(string FilePath, string Parameter)
        {
            try
            {
                Process.Start(FilePath, Parameter).WaitForExit();
            }
            catch (Exception e)
            {
                MessageBox.Show(e.Message.ToString() + ": " + FilePath, "Exception");
            }
        }

        public void StartNoWindow(string FilePath, string Parameter)
        {
            try
            {
                ProcessStartInfo ProcessInfo;
                Process Process;

                ProcessInfo = new ProcessStartInfo(FilePath, Parameter);
                ProcessInfo.CreateNoWindow = true;
                ProcessInfo.UseShellExecute = true;

                Process = Process.Start(ProcessInfo);
                Process.Dispose();
            }
            catch (Exception e)
            {
                MessageBox.Show(e.Message.ToString() + ": " + FilePath, "Exception");
            }
        }

        internal void Start(string v)
        {
            throw new NotImplementedException();
        }
    }
}