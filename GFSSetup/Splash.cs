﻿using System;
using System.Windows.Forms;

namespace GFSSetup
{
    public partial class Splash : Form
    {
        private const int CS_DROPSHADOW = 0x00020000;

        protected override CreateParams CreateParams
        {
            get
            {
                // add the drop shadow flag for automatically drawing
                // a drop shadow around the form
                CreateParams cp = base.CreateParams;
                cp.ClassStyle |= CS_DROPSHADOW;
                return cp;
            }
        }

        public Splash()
        {
            InitializeComponent();
        }

        private void timer1_Tick(object sender, EventArgs e)
        {
            if (timeLeft > 0)
            {
                timeLeft = timeLeft - 1;
            }
            else
            {
                timer1.Stop();
                Hide();
                Form1 form1 = new Form1();
                form1.ShowDialog();
                Close();
            }
        }

        private void Splash_Load(object sender, EventArgs e)
        {
            timeLeft = 40;
            timer1.Start();
        }

        public int timeLeft { get; set; }
    }
}