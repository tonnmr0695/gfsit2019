REM Last modified 10/1/16
REM Created by Michael Tonn
REM Note: This script will self remove after wallpaper has been set, user must have admin rights for the script to delete.
REM This script is created to set the first time wallpaper for new system deployments.

REM If username is qual to gfsadmin quit the script, else set the wallpaper for the current user then self destruct.
if "%username%" EQU "gfsadmin" (quit) Else (start C:\Users\Public\Documents\gfstheme.themepack
start "" "\\gfsit\shared\2019 Setup\SetUserFTA\SetUserFTA.exe" .pdf AcroExh.Document.DC
DEL "%~f0")