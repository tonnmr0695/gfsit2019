﻿namespace GFSSetup
{
    partial class PrintLabel
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.lollipopLabel1 = new LollipopLabel();
            this.comboBox1 = new System.Windows.Forms.ComboBox();
            this.SuspendLayout();
            // 
            // lollipopButton1
            // 
            this.lollipopButton1.Location = new System.Drawing.Point(229, 106);
            this.lollipopButton1.Text = "Print";
            this.lollipopButton1.Click += new System.EventHandler(this.lollipopButton1_Click);
            // 
            // lollipopButton2
            // 
            this.lollipopButton2.Location = new System.Drawing.Point(317, 106);
            this.lollipopButton2.Click += new System.EventHandler(this.lollipopButton2_Click);
            // 
            // lollipopLabel1
            // 
            this.lollipopLabel1.AutoSize = true;
            this.lollipopLabel1.BackColor = System.Drawing.Color.Transparent;
            this.lollipopLabel1.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F);
            this.lollipopLabel1.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(153)))), ((int)(((byte)(153)))), ((int)(((byte)(153)))));
            this.lollipopLabel1.Location = new System.Drawing.Point(12, 45);
            this.lollipopLabel1.Name = "lollipopLabel1";
            this.lollipopLabel1.Size = new System.Drawing.Size(387, 17);
            this.lollipopLabel1.TabIndex = 2;
            this.lollipopLabel1.Text = "Select your location to have a label printed for this computer";
            // 
            // comboBox1
            // 
            this.comboBox1.FormattingEnabled = true;
            this.comboBox1.Items.AddRange(new object[] {
            "IT WEST B1 (\\\\gfsprint1\\ITDEPTGX430t)",
            "IT EAST B3 (\\\\1W6MYW1-W10Y18\\ZDesigner GX420t)",
            "TEST"});
            this.comboBox1.Location = new System.Drawing.Point(15, 79);
            this.comboBox1.Name = "comboBox1";
            this.comboBox1.Size = new System.Drawing.Size(384, 21);
            this.comboBox1.TabIndex = 3;
            this.comboBox1.Text = "Select Printer...";
            // 
            // PrintLabel
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(424, 157);
            this.Controls.Add(this.comboBox1);
            this.Controls.Add(this.lollipopLabel1);
            this.Margin = new System.Windows.Forms.Padding(0, 0, 0, 0);
            this.Name = "PrintLabel";
            this.Text = "PrintLabel";
            this.Controls.SetChildIndex(this.lollipopButton2, 0);
            this.Controls.SetChildIndex(this.lollipopButton1, 0);
            this.Controls.SetChildIndex(this.lollipopLabel1, 0);
            this.Controls.SetChildIndex(this.comboBox1, 0);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private LollipopLabel lollipopLabel1;
        private System.Windows.Forms.ComboBox comboBox1;
    }
}