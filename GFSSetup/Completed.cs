﻿using System;

namespace GFSSetup
{
    public partial class Completed : BaseDialog
    {
        public Completed()
        {
            InitializeComponent();
        }

        private void lollipopButton1_Click(object sender, EventArgs e)
        {
            Hide();
            Form1 form1 = new Form1();
            form1.ShowDialog();
            Close();
        }

        private void lollipopButton2_Click(object sender, EventArgs e)
        {
            Close();
        }
    }
}