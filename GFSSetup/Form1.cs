﻿using GFSSetup.Class;
using Microsoft.Win32;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Diagnostics;
using System.Drawing;
using System.IO;
using System.Runtime.InteropServices;
using System.Windows.Forms;

namespace GFSSetup
{
    public partial class Form1 : Form
    {
        #region Create Instances

        private BackgroundWorker worker = new BackgroundWorker();

        #endregion Create Instances

        private string workingdir = Directory.GetCurrentDirectory();
        private string gfstmp = @"C:\gfstmp";

        #region FormEvents

        #region FormCustomizations

        private const int CS_DROPSHADOW = 0x00020000;

        protected override CreateParams CreateParams
        {
            get
            {
                // add the drop shadow flag for automatically drawing
                // a drop shadow around the form
                CreateParams cp = base.CreateParams;
                cp.ClassStyle |= CS_DROPSHADOW;
                return cp;
            }
        }

        protected override void OnLoad(EventArgs e)
        {
            this.AutoScaleDimensions = new System.Drawing.SizeF(96F, 96F);

            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Dpi;
            base.OnLoad(e);
        }

        private void changeImage(object sender, EventArgs e)
        {
            Image oldImage = this.BackgroundImage;

            List<Image> b1 = new List<Image>();
            b1.Add(Properties.Resources.b1); b1.Add(Properties.Resources.b2);
            b1.Add(Properties.Resources.b3); b1.Add(Properties.Resources.b4);
            b1.Add(Properties.Resources.b5); b1.Add(Properties.Resources.b6);
            b1.Add(Properties.Resources.b7); b1.Add(Properties.Resources.b8);
            int index = DateTime.Now.Second % 8;

            BackgroundImage = b1[index];

            oldImage.Dispose();
            //GC.Collect();
            //GC.WaitForPendingFinalizers();
        }

        #endregion FormCustomizations

        public Form1()
        {
            BackgroundImage = Properties.Resources.b4;
            InitializeComponent();

            Timer tm = new Timer();
            tm.Interval = 30000;
            tm.Tick += new EventHandler(changeImage);
            tm.Start();
        }

        private void TitleBar_MouseDown(object sender, MouseEventArgs e)
        {
            if (e.Button == MouseButtons.Left)
            {
                // Release the mouse capture started by the mouse down.
                panel2.Capture = false;

                // Create and send a WM_NCLBUTTONDOWN message.
                const int WM_NCLBUTTONDOWN = 0x00A1;
                const int HTCAPTION = 2;
                Message msg =
                    Message.Create(this.Handle, WM_NCLBUTTONDOWN,
                        new IntPtr(HTCAPTION), IntPtr.Zero);
                this.DefWndProc(ref msg);
            }
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            // Check if temp folder exists, if it doesn't create.
            Directory.CreateDirectory(@"C:\gfstmp");

            //If username is not gfsadmin alert them. 

            //if(Environment.UserName.ToLower() != "gfsadmin")
            //{
            //    MessageBox.Show("You must run this application under GFSADMIN \n to have full customizations during deployment!", "Wrong User", MessageBoxButtons.OK, MessageBoxIcon.Information);
            //    this.Close();

            //}

            //check is more than the allowed windows profile are on machine.
           
            int directoryCount = System.IO.Directory.GetDirectories(@"c:\users").Length;

            if (Environment.UserName.ToLower() == "gfsadmin")
            {

                int folderCount = Directory.GetDirectories(@"c:\users").Length;

                if (folderCount > 6)
                {
                    MessageBox.Show("More than one profile detected. Please clean up old profiles if needed.", "Profile Cleanup", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    Process.Start("rundll32", "sysdm.cpl, EditUserProfiles");
                }
            }


            // If system version is lower than Windows 7, alert user about unsupported version and close application.
            if (Environment.OSVersion.Version.Major <= 6 & Environment.OSVersion.Version.Minor <= 0)
            {
                MessageBox.Show("Unsupported Operating System", "Unsupported OS");
                this.Close();
            }

            // Checks windows current OS Build and if its less than Windows 10 1909 prompt to update.
            if (Environment.OSVersion.Version.Major == 10)
            {
                if (Environment.OSVersion.Version.Build < 18363)
                {
                    DialogResult result1 = MessageBox.Show("It's recommended you upgrade to Windows 10 November 2019 Update (1909). Would you like to update?", "Windows 10 Upgrade Available", MessageBoxButtons.YesNo);
                    if (result1 == DialogResult.Yes)
                    {
                        Process.Start(workingdir + @"\Microsoft\Windows10_1909");
                        this.Close();
                    }
                    else
                    {
                        TopMost = true;
                    }
                }
            }

            treeView1.Nodes[0].Expand();
            treeView1.Nodes[1].Expand();
            treeView1.Nodes[0].EnsureVisible();


            string[] args = Environment.GetCommandLineArgs();

            try
            {
                if (args[0] == "")
                {
                    Close();
                }
                else if (args[1] == "/s")
                {
                    DefaultInstall();
                    Close();
                }
                else
                {
                    Close();
                }
            }
            catch
            {
            }
    }

        private void Form1_FormClosing(object sender, FormClosingEventArgs e)
        {
            // Before form closes, check if gfstmp is on C:\. If exists delete before exit.
            if (Directory.Exists(gfstmp)) { Directory.Delete(gfstmp, true); }
        }

        private void InstallSelected_Click(object sender, EventArgs e)
        {
            bwInstall.WorkerReportsProgress = true; bwInstall.RunWorkerAsync();
        }

        private void FormClose_Click(object sender, EventArgs e) => Close();

        #endregion FormEvents

        #region Methods

        public void Autodesk()
        {
            var process = new GfsStartProcess();
            gfsInvoke("Installing DWG Trueview 2020");
            if (Directory.Exists(@"C:\Program Files\Autodesk") == false)
            {
                process.Start(workingdir + @"\Autodesk\DWGTrueView_2020_Enu_64bit_dlm\setup.exe", "/qb /norestart /w /t setup.ini /language en-us");
            }

            gfsInvoke("Installing Autodesk Design Review 2018");
            try
            {
                process.Start(workingdir + @"\Autodesk\DesignReview_2018_ENU_64bit\Setup.exe", "/qb /norestart /w /t setup.ini /language en-us");
            }
            catch
            {
            }
        }

        public void CBProtectionAgent()
        {
            var web = new GfsWeb();
            gfsInvoke("Installing CB Agent");
            web.Request("https://cbprotect.global.com/hostpkg/pkg.php?pkg=gfs%20-%20disabled%20install.msi", @"C:\gfstmp\gfs - disabled install.msi");

            try
            {
                Process.Start(gfstmp + @"\gfs - disabled install.msi", "/quiet").WaitForExit();
            }
            catch
            {
            }
        }


        public void ArcticWolfAgent()
        {
            var web = new GfsWeb();
            gfsInvoke("Installing ArticWolf Agent");
            try
            {
                Process.Start(workingdir + @"\ArcticWolf-Agent\arcticwolfagent-2019-09_03.msi", "/q").WaitForExit();
            }
            catch
            {
            }
        }

        public void DefaultDesktopShortcuts()
        {
            var file = new GfsFile();
            gfsInvoke("Copying default shortcuts");
            file.Copy(FilePath: workingdir + @"\Shortcuts\drmap.cmd", FileDestination: @"C:\Users\Public\Desktop\drmap.cmd");
            file.Copy(FilePath: workingdir + @"\Shortcuts\gfsprint.lnk", FileDestination: @"C:\Users\Public\Desktop\gfsprint.lnk");
            file.Copy(FilePath: workingdir + @"\Shortcuts\TimeClockPlus.website", FileDestination: @"C:\Users\Public\Desktop\TimeClockPlus.website");
        }

        public void DefaultRegSettings()
        {
            gfsInvoke("Enabling Remote Desktop");
            Registry.SetValue(@"HKEY_LOCAL_MACHINE\SYSTEM\CurrentControlSet\Control\Terminal Server", "fDenyTSConnections", "0", RegistryValueKind.DWord);
            gfsInvoke("Setting IE Homepage");
            Registry.SetValue(@"HKEY_CURRENT_USER\SOFTWARE\MICROSOFT\INTERNET EXPLORER\MAIN", "Start Page", "https://www.google.com", RegistryValueKind.String);
            gfsInvoke("Settings firewall rules");
            Process.Start("netsh", "advfirewall firewall set rule group=\"remote desktop\" new enable=yes").WaitForExit();
            gfsInvoke("Adjusting Timezone");
            Process.Start("tzutil", "/s \"Central Standard Time\"").WaitForExit();
            gfsInvoke("Disable Notification Center");
            Registry.SetValue(@"HKEY_CURRENT_USER\Software\Policies\Microsoft\Windows\Explorer", "DisableNotificationCenter", 1, RegistryValueKind.DWord);

            Process[] proclist = Process.GetProcessesByName("explorer");
            foreach (Process p in proclist)
            {
                p.Kill();
            }
            gfsInvoke("Enabling desktop shortcuts");
            Registry.SetValue(@"HKEY_CURRENT_USER\Software\Microsoft\Windows\CurrentVersion\Explorer\HideDesktopIcons\NewStartPanel", "{20D04FE0-3AEA-1069-A2D8-08002B30309D}", 0, RegistryValueKind.DWord);
            Registry.SetValue(@"HKEY_CURRENT_USER\Software\Microsoft\Windows\CurrentVersion\Explorer\HideDesktopIcons\ClassicStartMenu", "{20D04FE0-3AEA-1069-A2D8-08002B30309D}", 0, RegistryValueKind.DWord);
            Registry.SetValue(@"HKEY_LOCAL_MACHINE\SOFTWARE\Microsoft\Windows\CurrentVersion\Explorer\HideDesktopIcons\NewStartPanel", "{20D04FE0-3AEA-1069-A2D8-08002B30309D}", 0, RegistryValueKind.DWord);
            Registry.SetValue(@"HKEY_LOCAL_MACHINE\SOFTWARE\Microsoft\Windows\CurrentVersion\Explorer\HideDesktopIcons\ClassicStartMenu", "{20D04FE0-3AEA-1069-A2D8-08002B30309D}", 0, RegistryValueKind.DWord);

            Registry.SetValue(@"HKEY_CURRENT_USER\Software\Microsoft\Windows\CurrentVersion\Explorer\HideDesktopIcons\NewStartPanel", "{5399E694-6CE5-4D6C-8FCE-1D8870FDCBA0}", 0, RegistryValueKind.DWord);
            Registry.SetValue(@"HKEY_CURRENT_USER\Software\Microsoft\Windows\CurrentVersion\Explorer\HideDesktopIcons\ClassicStartMenu", "{5399E694-6CE5-4D6C-8FCE-1D8870FDCBA0}", 0, RegistryValueKind.DWord);
            Registry.SetValue(@"HKEY_LOCAL_MACHINE\SOFTWARE\Microsoft\Windows\CurrentVersion\Explorer\HideDesktopIcons\NewStartPanel", "{5399E694-6CE5-4D6C-8FCE-1D8870FDCBA0}", 0, RegistryValueKind.DWord);
            Registry.SetValue(@"HKEY_LOCAL_MACHINE\SOFTWARE\Microsoft\Windows\CurrentVersion\Explorer\HideDesktopIcons\ClassicStartMenu", "{5399E694-6CE5-4D6C-8FCE-1D8870FDCBA0}", 0, RegistryValueKind.DWord);

            Registry.SetValue(@"HKEY_CURRENT_USER\Software\Microsoft\Windows\CurrentVersion\Explorer\HideDesktopIcons\NewStartPanel", "{F02C1A0D-BE21-4350-88B0-7367FC96EF3C}", 0, RegistryValueKind.DWord);
            Registry.SetValue(@"HKEY_CURRENT_USER\Software\Microsoft\Windows\CurrentVersion\Explorer\HideDesktopIcons\ClassicStartMenu", "{F02C1A0D-BE21-4350-88B0-7367FC96EF3C}", 0, RegistryValueKind.DWord);
            Registry.SetValue(@"HKEY_LOCAL_MACHINE\SOFTWARE\Microsoft\Windows\CurrentVersion\Explorer\HideDesktopIcons\NewStartPanel", "{F02C1A0D-BE21-4350-88B0-7367FC96EF3C}", 0, RegistryValueKind.DWord);
            Registry.SetValue(@"HKEY_LOCAL_MACHINE\SOFTWARE\Microsoft\Windows\CurrentVersion\Explorer\HideDesktopIcons\ClassicStartMenu", "{F02C1A0D-BE21-4350-88B0-7367FC96EF3C}", 0, RegistryValueKind.DWord);

            Registry.SetValue(@"HKEY_CURRENT_USER\Software\Microsoft\Windows\CurrentVersion\Explorer\HideDesktopIcons\NewStartPanel", "{645FF040-5081-101B-9F08-00AA002F954E}", 0, RegistryValueKind.DWord);
            Registry.SetValue(@"HKEY_CURRENT_USER\Software\Microsoft\Windows\CurrentVersion\Explorer\HideDesktopIcons\ClassicStartMenu", "{645FF040-5081-101B-9F08-00AA002F954E}", 0, RegistryValueKind.DWord);
            Registry.SetValue(@"HKEY_LOCAL_MACHINE\SOFTWARE\Microsoft\Windows\CurrentVersion\Explorer\HideDesktopIcons\NewStartPanel", "{645FF040-5081-101B-9F08-00AA002F954E}", 0, RegistryValueKind.DWord);
            Registry.SetValue(@"HKEY_LOCAL_MACHINE\SOFTWARE\Microsoft\Windows\CurrentVersion\Explorer\HideDesktopIcons\ClassicStartMenu", "{645FF040-5081-101B-9F08-00AA002F954E}", 0, RegistryValueKind.DWord);

            Registry.SetValue(@"HKEY_CURRENT_USER\Software\Microsoft\Windows\CurrentVersion\Explorer\HideDesktopIcons\NewStartPanel", "{59031a47-3f72-44a7-89c5-5595fe6b30ee}", 0, RegistryValueKind.DWord);
            Registry.SetValue(@"HKEY_CURRENT_USER\Software\Microsoft\Windows\CurrentVersion\Explorer\HideDesktopIcons\ClassicStartMenu", "{59031a47-3f72-44a7-89c5-5595fe6b30ee}", 0, RegistryValueKind.DWord);
            Registry.SetValue(@"HKEY_LOCAL_MACHINE\SOFTWARE\Microsoft\Windows\CurrentVersion\Explorer\HideDesktopIcons\NewStartPanel", "{59031a47-3f72-44a7-89c5-5595fe6b30ee}", 0, RegistryValueKind.DWord);
            Registry.SetValue(@"HKEY_LOCAL_MACHINE\SOFTWARE\Microsoft\Windows\CurrentVersion\Explorer\HideDesktopIcons\ClassicStartMenu", "{59031a47-3f72-44a7-89c5-5595fe6b30ee}", 0, RegistryValueKind.DWord);
        }

        public void DoPDF10()
        {
            gfsInvoke("Installing doPDF10 Full");
            try
            {
                Process.Start(workingdir + @"\doPDF\doPDF-full.exe", "/silent /norestart").WaitForExit(); ;
            }
            catch
            {
            }
        }

        public void FortiClientSSL()
        {
            var process = new GfsStartProcess();
            gfsInvoke("Installing Forticlient");

            File.WriteAllBytes(gfstmp + @"\forticlientsettings.conf", GFSSetup.Properties.Resources.forticlientsettings);
            process.Start(workingdir + @"\Forticlient\FortiClient.msi", "/quiet /norestart INSTALLLEVEL=3");

            // Edit the config to set current logged in user, Reads all lines in file then edits line number with logged in username
            //var username = Environment.UserName;
            //var lines = File.ReadAllLines(gfstmp + @"\forticlientsettings.conf");
            //lines[147] = "<username>" + "" + "</username>";
            //File.WriteAllLines(gfstmp + @"\forticlientsettings.conf", lines);
            if (File.Exists(gfstmp + @"\forticlientsettings.conf"))
            {
                process.Start(@"C:\Program Files\Fortinet\Forticlient\fcconfig.exe", @"-m all -f c:\gfstmp\forticlientsettings.conf -o import -i 1");
            }
        }

        public void GFSThemePack()
        {
            gfsInvoke("Copying GFS Themepack");
            try
            {
                File.Copy(workingdir + @"\wallpaper\gfstheme.themepack", @"C:\Users\Public\Documents\gfstheme.themepack", true);
            }
            catch
            {
            }
            File.WriteAllText(@"C:\ProgramData\Microsoft\Windows\Start Menu\Programs\Startup\wallpaper.bat", GFSSetup.Properties.Resources.wallpaper);
        }

        public void GreenHeckCaps()
        {
            
            // Set caps setup directory

            var web = new GfsWeb();
            gfsInvoke("Installing CAPS");
            web.Request("http://cfs.greenops.com/deploy/Partner/CapsPartnerSetup.exe", @"C:\gfstmp\CapsPartnerSetup.exe");

            try
            {
                Process.Start(gfstmp + @"\CapsPartnerSetup.exe", "/q").WaitForExit();
            }
            catch
            {
            }

            
        }

        #region SetForegroundWindow for IFS Installer

        [DllImport("User32.dll")]
        private static extern int SetForegroundWindow(IntPtr point);

        #endregion SetForegroundWindow for IFS Installer

        public void IFSApplications()
        {
            gfsInvoke("Installing IFS8 Applications");
            // Set IFS8 to the location path for OracleInstaller

            string IFS8Shortcuts = @"\\gfsit\shared\IFS Oracle Installation\IFS Install\batIFS8";

            try
            {
                Process.Start(workingdir + @"\SetupOracle\SetupOracle.exe" , "/s").WaitForExit();

            }
            catch (Exception e)
            {
                MessageBox.Show(e.ToString(), "Exception");
            }


            var file = new GfsFile();
            gfsInvoke("Copying IFS8 Shortcuts");
            file.Copy(IFS8Shortcuts + @"\IFS 8 - PROD.url", @"C:\Users\Public\Desktop\IFS 8 - PROD.url");
            file.Copy(IFS8Shortcuts + @"\GFS_SCHEDULE.lnk", @"C:\Users\Public\Desktop\GFS_SCHEDULE.lnk");

            // Install Microsoft Visual C++ 2010 SP1 Redistributable Package (x64), required for Prepare Work Order Confirmation and Purchase Order right-click emails.
            gfsInvoke("Installing Microsoft Visual C++ 2010 SP1 Redistributable Package (x64)");
            Process.Start(workingdir + @"\Microsoft\Redistributable\Microsoft Visual C++ 2010 SP1 Redistributable Package (x64).exe", "/q /norestart").WaitForExit();
        }

        public void O365ProPlus()
        {
            bwInstall.ReportProgress(40);
            gfsInvoke("Removing Existing Office Installs");
            Process.Start(workingdir + @"\Microsoft\Access_Database_Engine_2010\uninstall.bat").WaitForExit();
            Process.Start(workingdir + @"\Microsoft\O365ProPlusRetailx32\OffScrub365.vbs").WaitForExit(); ;
            gfsInvoke("Installing Office 365 ProPlus");
            Process.Start(workingdir + @"\Microsoft\O365ProPlusRetailx32\setup.bat").WaitForExit();

            var file = new GfsFile();
            file.Copy(@"C:\ProgramData\Microsoft\Windows\Start Menu\Programs\Outlook.lnk", @"C:\Users\Public\Desktop\Outlook.lnk");

            try
            {
                if (Environment.OSVersion.Version.Major == 10)
                {
                    if (File.Exists(gfstmp + @"\layout2016.exe"))
                    {
                        File.Delete(gfstmp + @"\layout2016.exe");
                    }

                    File.WriteAllBytes(@"C:\gfstmp\layout2016.exe", GFSSetup.Properties.Resources.layout2016);
                    Process.Start(gfstmp + @"\layout2016.exe").WaitForExit();
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        public void OfficeVL()
        {
            var process = new GfsStartProcess();
            var file = new GfsFile();

            bwInstall.ReportProgress(45);
            gfsInvoke("Installing Office VL 2013");
            process.Start(workingdir + @"\Microsoft\MicrosoftOfficeVL2013SP1\Setup.exe", "/adminfile adminfile.msp");

            if (Environment.OSVersion.Version.Major == 10)
            {
                if (File.Exists(@"C:\ProgramData\Microsoft\Windows\Start Menu\Programs\Microsoft Office 2013\Outlook 2013.lnk"))
                {
                    file.Copy(@"C:\ProgramData\Microsoft\Windows\Start Menu\Programs\Microsoft Office 2013\Outlook 2013.lnk", @"C:\Users\Public\Desktop\Outlook 2013.lnk");

                    File.WriteAllBytes(gfstmp + @"\layout2013.exe", GFSSetup.Properties.Resources.layout2013);

                    if (File.Exists(gfstmp + @"\layout2013.exe"))
                    {
                        Process.Start(gfstmp + @"\layout2013.exe");
                    }
                }
            }
        }

        public void ThirdPartyApps()
        {
            var process = new GfsStartProcess();
            var web = new GfsWeb();
            var file = new GfsFile();
            gfsInvoke("Installing 3rd Party Apps");
            file.GetFileType(workingdir + @"\3rdPartyApps", "msi", "/qn");
        }

        public void Project2016STDVL()
        {
            var file = new GfsFile();
            bwInstall.ReportProgress(45);
            gfsInvoke("Installing Project 2016 VL");
            if (File.Exists(@"C:\Program Files\Microsoft Office\root\Office16\WINPROJ.EXE") == false)
            {
                Process.Start(workingdir + @"\Microsoft\ProjectStdXVolumex32\setup.bat").WaitForExit();
            }

            if (File.Exists(@"C:\users\public\desktop\Project 2016.lnk") == false)
            {
                file.Copy(@"C:\ProgramData\Microsoft\Windows\Start Menu\Programs\Project 2016.lnk", @"c:\users\public\desktop\Project 2016.lnk");
            }
        }

        public void RuppAirNew()
        {
            var file = new GfsFile();
            file.Copy(workingdir + @"\Shortcuts\NOLA - GFS RUPPAIR.url", @"C:\Users\Public\Desktop\NOLA - GFS RUPPAIR.url");
        }

        public void Log()
        {
            GfsFile logcsv = new GfsFile();
            logcsv.LogCSV(workingdir + @"\log.txt", Environment.MachineName.ToString(), DateTime.Now.ToString());
        }

        public void ShopAddons()
        {
            var process = new GfsStartProcess();

            bwInstall.ReportProgress(45);

            gfsInvoke("Installing IDAutomation Code 39 Fonts");

            File.Copy(workingdir + @"\ShopDrawings\tabctl32.ocx", @"C:\Windows\tabctl32.ocx", true);
            File.Copy(workingdir + @"\ShopDrawings\tabctl32.ocx", @"C:\Windows\SysWOW64\tabctl32.ocx", true);
            try
            {
                process.StartNoWindow("cmd.exe", "/C " + @"regsvr32.exe /s C:\Windows\tabctl32.ocx && regsvr32.exe /s C:\Windows\SysWOW64\tabctl32.ocx");
            }
            catch
            {
            }
            File.Copy(workingdir + @"\ShopDrawings\ShopDrawing.lnk", @"C:\Users\Public\Desktop\ShopDrawing.lnk", true);

            gfsInvoke("Installing IDAutomation Code39 Font");
            try
            {
                Process.Start(@"\\gfsit\shared\IFS Oracle Installation\IDAutomationFonts\IDAutomation Code 39 Font Advantage\IDAutomation_C39FontAdvantage.exe").WaitForExit();
            }
            catch
            {
            }

            gfsInvoke("Installing Zebra Utilities");
            try
            {
                Process.Start(workingdir + @"\ZebraUtilities\zsu-1191286.exe", "/s").WaitForExit();
            }
            catch
            {
            }
        }

        public void Visio2016STDVL()
        {
            var file = new GfsFile();
            bwInstall.ReportProgress(45);
            gfsInvoke("Installing Visio 2016 VL");
            if (File.Exists(@"C:\Program Files\Microsoft Office\root\Office16\VISIO.EXE") == false)
            {
                Process.Start(workingdir + @"\Microsoft\VisioStdXVolumex32\setup.bat").WaitForExit(); ;
            }
            file.Copy(@"C:\ProgramData\Microsoft\Windows\Start Menu\Programs\Visio 2016.lnk", @"c:\users\public\desktop\Visio 2016.lnk");
        }

        //public void UPerform()
        //{
        //    var file = new GfsFile();
        //    bwInstall.ReportProgress(56);
        //    gfsInvoke("Installing Ancile uPerform");

        //    file.Copy(@"\\gfsit.global.com\shared\Ancile\uPerformInstall.bat", Environment.GetFolderPath(Environment.SpecialFolder.Desktop) + @"\uperformInstall.bat");
        //    try
        //    {
        //        Process.Start(Environment.GetFolderPath(Environment.SpecialFolder.Desktop) + @"\uPerformInstall.bat").WaitForExit();
        //    }
        //    catch
        //    {
        //    }
        //    if (File.Exists(Environment.GetFolderPath(Environment.SpecialFolder.Desktop) + @"\uPerformInstall.bat"))
        //    {
        //        File.Delete(Environment.GetFolderPath(Environment.SpecialFolder.Desktop) + @"\uPerformInstall.bat");
        //    }
        //}


        public void CiscoJabber()
        {
            gfsInvoke("Installing Cisco Jabber");
            try
            {
                Process.Start(workingdir + @"\Cisco\Jabber\CiscoJabberSetup.msi", "/q").WaitForExit();
            }
            catch (Exception e)
            {
                MessageBox.Show("Exception", e.ToString());
            }
        }

        public void GlobalSearch()
        {
            var file = new GfsFile();
            bwInstall.ReportProgress(56);
            gfsInvoke("Installing Global Search");
            file.Copy(@"\\gfsit.global.com\shared\SmartSearch\ClientInstall\SmartSearchInstall - WIN10.bat", Environment.GetFolderPath(Environment.SpecialFolder.Desktop) + @"\SmartSearchInstall - WIN10.bat");
            try
            {
                Process.Start(Environment.GetFolderPath(Environment.SpecialFolder.Desktop) + @"\SmartSearchInstall - WIN10.bat").WaitForExit();
            }
            catch
            {
            }
            if (File.Exists(Environment.GetFolderPath(Environment.SpecialFolder.Desktop) + @"\SmartSearchInstall - WIN10.bat"))
            {
                File.Delete(Environment.GetFolderPath(Environment.SpecialFolder.Desktop) + @"\SmartSearchInstall - WIN10.bat");
            }
        }

        public void Windows10Settings()
        {
            var process = new GfsStartProcess();
            var file = new GfsFile();

            gfsInvoke("Applying Power Settings");
            process.Start("cmd", "/C powercfg.exe -change -monitor-timeout-ac 0");
            process.Start("cmd", "/C powercfg.exe -change -standby-timeout-ac 0");
            process.Start("cmd", "/C powercfg.exe /h off");

            gfsInvoke("Disabling Cortona");
            Registry.SetValue(@"HKEY_LOCAL_MACHINE\SOFTWARE\Policies\Microsoft\Windows\Windows Search", "AllowCortana", 0, RegistryValueKind.DWord);

            gfsInvoke("Disable Suggested Apps StartMenu");
            Registry.SetValue(@"HKEY_LOCAL_MACHINE\SOFTWARE\Policies\Microsoft\Windows\CloudContent", "DisableWindowsConsumerFeatures", 1, RegistryValueKind.DWord);

            gfsInvoke("Applying Custom Layout");
            file.Copy(workingdir + @"\layout\StartLayoutOffice2013.xml", @"C:\Users\Public\Documents\StartLayoutOffice2013.xml");
            file.Copy(workingdir + @"\layout\StartLayoutOffice2016.xml", @"C:\Users\Public\Documents\StartLayoutOffice2016.xml");

            gfsInvoke("Copying IE shortcut to start menu");
            File.Copy(Environment.GetFolderPath(Environment.SpecialFolder.StartMenu) + @"\Programs\Accessories\Internet Explorer.lnk", Environment.GetFolderPath(Environment.SpecialFolder.CommonStartMenu) + @"\Programs\Internet Explorer.lnk", true);

            try
            {
                foreach (Process proc in Process.GetProcessesByName("OneDrive"))
                {
                    proc.Kill();
                }

                process.Start(@"C:\Windows\SysWOW64\OneDriveSetup.exe", "/uninstall");
            }
            catch (Exception e)
            {
                MessageBox.Show("Exception", e.ToString());
            }
        }

        public void Windows7Settings()
        {
            var process = new GfsStartProcess();
            gfsInvoke("Setting firewall rules");
            process.Start("cmd", "/C netsh.exe advfirewall set allprofiles state off");
            Registry.SetValue(@"HKEY_LOCAL_MACHINE\Software\Microsoft\Windows\CurrentVersion\Policies\System", "EnableLUA", "0", RegistryValueKind.DWord);

            process.Start("cmd", "/C powercfg.exe -change -monitor-timeout-ac 0");
            process.Start("cmd", "/C powercfg.exe -change -standby-timeout-ac 0");
        }

        public void gfsInvoke(string message)
        {
            Invoke((MethodInvoker)delegate
            {
                status.Text = message;
            });
        }

        public void FanSelectorFS10()
        {
            gfsInvoke("Installing Fan Selector (FS10)");
            Process.Start(workingdir + @"\FanSelector\FS10Setup_10.01.exe").WaitForExit();
        }

        private void RecurseTree(TreeNode node)
        {
            if (node.Checked == true)
            {
                switch (node.Name)
                {
                    case "StandardSetup":
                        var os = Environment.OSVersion.Version;

                        if (os.Major == 10 || os.Major > 6 && os.Minor >= 2)
                        {
                            Windows10Settings();
                            DefaultInstall();
                        }
                        else if (os.Major == 6 | os.Minor >= 1)
                        {
                            Windows7Settings();
                            DefaultInstall();
                        }
                        break;

                    case "CiscoJabber":
                        CiscoJabber();
                        break;

                    case "ForticlientSSLVPN":
                        FortiClientSSL();
                        break;

                    case "GreenheckCaps":
                        GreenHeckCaps();
                        break;

                    case "MSOffice2013VL":
                        OfficeVL();
                        break;

                    case "Office365ProPlus":
                        O365ProPlus();
                        break;

                    case "ProjectSTD2016VL":
                        Project2016STDVL();
                        break;

                    case "RuppAir":
                        RuppAirNew();
                        break;

                    case "ShopAddons":
                        ShopAddons();
                        break;

                    case "VisioSTD2016VL":
                        Visio2016STDVL();
                        break;

                    case "GlobalSearch":
                        GlobalSearch();
                        break;

                    case "IFSApplications":
                        IFSApplications();
                        break;

                    case "FanSelectorFS10":
                        FanSelectorFS10();
                        break;

                    default:
                        MessageBox.Show("error");
                        break;
                }
            }

            foreach (TreeNode childNode in node.Nodes) RecurseTree(childNode);
        }

        public void DefaultInstall()
        {
            bwInstall.ReportProgress(5);
            DefaultRegSettings();
            bwInstall.ReportProgress(10);
            DefaultDesktopShortcuts();
            bwInstall.ReportProgress(18);
            Autodesk();
            bwInstall.ReportProgress(20);
            ThirdPartyApps();
            bwInstall.ReportProgress(25);
            DoPDF10();
            bwInstall.ReportProgress(32);
            CBProtectionAgent();
            bwInstall.ReportProgress(33);
            ArcticWolfAgent();
            bwInstall.ReportProgress(35);
            GFSThemePack();
        }

        #endregion Methods

        #region BackgroundWorker

        private void BwInstall_DoWork(object sender, DoWorkEventArgs e)
        {
            foreach (TreeNode node in treeView1.Nodes) RecurseTree(node);
            Log();
            bwInstall.ReportProgress(100);
        }

        private void BwInstall_ProgressChanged(object sender, ProgressChangedEventArgs e) => lollipopProgressBar1.Value = e.ProgressPercentage;

        private void BwInstall_RunWorkerCompleted(object sender, RunWorkerCompletedEventArgs e)
        {
            gfsInvoke("");
            Hide();
            Completed completed = new Completed();
            completed.ShowDialog();
            Close();
        }

        #endregion BackgroundWorker

    }
}