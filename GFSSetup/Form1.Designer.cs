﻿
namespace GFSSetup
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.Windows.Forms.TreeNode treeNode1 = new System.Windows.Forms.TreeNode("Shop Addons");
            System.Windows.Forms.TreeNode treeNode2 = new System.Windows.Forms.TreeNode("Standard Setup");
            System.Windows.Forms.TreeNode treeNode3 = new System.Windows.Forms.TreeNode("Install Profiles", new System.Windows.Forms.TreeNode[] {
            treeNode1,
            treeNode2});
            System.Windows.Forms.TreeNode treeNode4 = new System.Windows.Forms.TreeNode("Cisco Jabber");
            System.Windows.Forms.TreeNode treeNode5 = new System.Windows.Forms.TreeNode("Fan Selector (FS10)");
            System.Windows.Forms.TreeNode treeNode6 = new System.Windows.Forms.TreeNode("Forticlient SSL VPN");
            System.Windows.Forms.TreeNode treeNode7 = new System.Windows.Forms.TreeNode("Greenheck Caps");
            System.Windows.Forms.TreeNode treeNode8 = new System.Windows.Forms.TreeNode("IFS Applications");
            System.Windows.Forms.TreeNode treeNode9 = new System.Windows.Forms.TreeNode("Microsoft Office 2013 (VL)");
            System.Windows.Forms.TreeNode treeNode10 = new System.Windows.Forms.TreeNode("Microsoft Office 365 - ProPlus");
            System.Windows.Forms.TreeNode treeNode11 = new System.Windows.Forms.TreeNode("Microsoft Project Standard 2016 (VL)");
            System.Windows.Forms.TreeNode treeNode12 = new System.Windows.Forms.TreeNode("Microsoft Visio Standard 2016 (VL)");
            System.Windows.Forms.TreeNode treeNode13 = new System.Windows.Forms.TreeNode("Rupp Air");
            System.Windows.Forms.TreeNode treeNode14 = new System.Windows.Forms.TreeNode("Global Search");
            System.Windows.Forms.TreeNode treeNode15 = new System.Windows.Forms.TreeNode("Other Applications", new System.Windows.Forms.TreeNode[] {
            treeNode4,
            treeNode5,
            treeNode6,
            treeNode7,
            treeNode8,
            treeNode9,
            treeNode10,
            treeNode11,
            treeNode12,
            treeNode13,
            treeNode14});
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Form1));
            this.panel1 = new System.Windows.Forms.Panel();
            this.treeView1 = new System.Windows.Forms.TreeView();
            this.status = new System.Windows.Forms.Label();
            this.lollipopProgressBar1 = new LollipopProgressBar();
            this.lollipopButton1 = new LollipopButton();
            this.panel2 = new System.Windows.Forms.Panel();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.bwInstall = new System.ComponentModel.BackgroundWorker();
            this.Progressbar = new System.Windows.Forms.Timer(this.components);
            this.toolTip1 = new System.Windows.Forms.ToolTip(this.components);
            this.lblGlobalFinishing = new System.Windows.Forms.Label();
            this.lblSoftwareDeployment = new System.Windows.Forms.Label();
            this.lollipopLabel2 = new LollipopLabel();
            this.panel1.SuspendLayout();
            this.panel2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            this.SuspendLayout();
            // 
            // panel1
            // 
            this.panel1.BackColor = System.Drawing.Color.WhiteSmoke;
            this.panel1.Controls.Add(this.treeView1);
            this.panel1.Controls.Add(this.status);
            this.panel1.Controls.Add(this.lollipopProgressBar1);
            this.panel1.Controls.Add(this.lollipopButton1);
            this.panel1.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.panel1.Location = new System.Drawing.Point(0, 513);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(329, 252);
            this.panel1.TabIndex = 2;
            // 
            // treeView1
            // 
            this.treeView1.BackColor = System.Drawing.Color.WhiteSmoke;
            this.treeView1.CheckBoxes = true;
            this.treeView1.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.treeView1.ForeColor = System.Drawing.SystemColors.MenuText;
            this.treeView1.Location = new System.Drawing.Point(12, 36);
            this.treeView1.Name = "treeView1";
            treeNode1.Name = "ShopAddons";
            treeNode1.Text = "Shop Addons";
            treeNode2.Checked = true;
            treeNode2.Name = "StandardSetup";
            treeNode2.Text = "Standard Setup";
            treeNode3.Name = "Node1";
            treeNode3.Text = "Install Profiles";
            treeNode4.Name = "CiscoJabber";
            treeNode4.Text = "Cisco Jabber";
            treeNode5.Name = "FanSelectorFS10";
            treeNode5.Text = "Fan Selector (FS10)";
            treeNode6.Name = "ForticlientSSLVPN";
            treeNode6.Text = "Forticlient SSL VPN";
            treeNode7.Name = "GreenheckCaps";
            treeNode7.Text = "Greenheck Caps";
            treeNode8.Checked = true;
            treeNode8.Name = "IFSApplications";
            treeNode8.Text = "IFS Applications";
            treeNode9.Name = "MSOffice2013VL";
            treeNode9.Text = "Microsoft Office 2013 (VL)";
            treeNode10.Checked = true;
            treeNode10.Name = "Office365ProPlus";
            treeNode10.Text = "Microsoft Office 365 - ProPlus";
            treeNode11.Name = "ProjectSTD2016VL";
            treeNode11.Text = "Microsoft Project Standard 2016 (VL)";
            treeNode12.Name = "VisioSTD2016VL";
            treeNode12.Text = "Microsoft Visio Standard 2016 (VL)";
            treeNode13.Name = "RuppAir";
            treeNode13.Text = "Rupp Air";
            treeNode14.Name = "GlobalSearch";
            treeNode14.Text = "Global Search";
            treeNode15.Name = "Node2";
            treeNode15.Text = "Other Applications";
            this.treeView1.Nodes.AddRange(new System.Windows.Forms.TreeNode[] {
            treeNode3,
            treeNode15});
            this.treeView1.Size = new System.Drawing.Size(305, 165);
            this.treeView1.TabIndex = 16;
            // 
            // status
            // 
            this.status.AutoSize = true;
            this.status.Font = new System.Drawing.Font("Segoe UI", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.status.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(132)))), ((int)(((byte)(22)))), ((int)(((byte)(36)))));
            this.status.Location = new System.Drawing.Point(4, 13);
            this.status.Name = "status";
            this.status.Size = new System.Drawing.Size(0, 13);
            this.status.TabIndex = 2;
            // 
            // lollipopProgressBar1
            // 
            this.lollipopProgressBar1.BGColor = "#BD1D31";
            this.lollipopProgressBar1.Location = new System.Drawing.Point(0, -5);
            this.lollipopProgressBar1.Name = "lollipopProgressBar1";
            this.lollipopProgressBar1.Size = new System.Drawing.Size(329, 10);
            this.lollipopProgressBar1.TabIndex = 14;
            this.lollipopProgressBar1.Text = "lollipopProgressBar1";
            this.lollipopProgressBar1.Value = 0;
            // 
            // lollipopButton1
            // 
            this.lollipopButton1.BackColor = System.Drawing.Color.Transparent;
            this.lollipopButton1.BGColor = "#BD1D31";
            this.lollipopButton1.FontColor = "#ffffff";
            this.lollipopButton1.Location = new System.Drawing.Point(109, 207);
            this.lollipopButton1.Name = "lollipopButton1";
            this.lollipopButton1.Size = new System.Drawing.Size(98, 33);
            this.lollipopButton1.TabIndex = 13;
            this.lollipopButton1.Text = "Install Selected";
            this.lollipopButton1.Click += new System.EventHandler(this.InstallSelected_Click);
            // 
            // panel2
            // 
            this.panel2.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(195)))), ((int)(((byte)(32)))), ((int)(((byte)(50)))));
            this.panel2.Controls.Add(this.pictureBox1);
            this.panel2.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel2.Location = new System.Drawing.Point(0, 0);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(329, 30);
            this.panel2.TabIndex = 5;
            this.panel2.MouseDown += new System.Windows.Forms.MouseEventHandler(this.TitleBar_MouseDown);
            // 
            // pictureBox1
            // 
            this.pictureBox1.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("pictureBox1.BackgroundImage")));
            this.pictureBox1.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.pictureBox1.Location = new System.Drawing.Point(298, 1);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(28, 26);
            this.pictureBox1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBox1.TabIndex = 0;
            this.pictureBox1.TabStop = false;
            this.pictureBox1.Click += new System.EventHandler(this.FormClose_Click);
            // 
            // bwInstall
            // 
            this.bwInstall.DoWork += new System.ComponentModel.DoWorkEventHandler(this.BwInstall_DoWork);
            this.bwInstall.ProgressChanged += new System.ComponentModel.ProgressChangedEventHandler(this.BwInstall_ProgressChanged);
            this.bwInstall.RunWorkerCompleted += new System.ComponentModel.RunWorkerCompletedEventHandler(this.BwInstall_RunWorkerCompleted);
            // 
            // lblGlobalFinishing
            // 
            this.lblGlobalFinishing.AutoSize = true;
            this.lblGlobalFinishing.BackColor = System.Drawing.Color.Transparent;
            this.lblGlobalFinishing.Font = new System.Drawing.Font("Impact", 20.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblGlobalFinishing.ForeColor = System.Drawing.Color.White;
            this.lblGlobalFinishing.Location = new System.Drawing.Point(74, 354);
            this.lblGlobalFinishing.Name = "lblGlobalFinishing";
            this.lblGlobalFinishing.Size = new System.Drawing.Size(192, 34);
            this.lblGlobalFinishing.TabIndex = 0;
            this.lblGlobalFinishing.Text = "Global Finishing";
            // 
            // lblSoftwareDeployment
            // 
            this.lblSoftwareDeployment.AutoSize = true;
            this.lblSoftwareDeployment.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(32)))), ((int)(((byte)(31)))), ((int)(((byte)(31)))));
            this.lblSoftwareDeployment.Font = new System.Drawing.Font("Impact", 20.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblSoftwareDeployment.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(132)))), ((int)(((byte)(22)))), ((int)(((byte)(36)))));
            this.lblSoftwareDeployment.Location = new System.Drawing.Point(37, 427);
            this.lblSoftwareDeployment.Name = "lblSoftwareDeployment";
            this.lblSoftwareDeployment.Size = new System.Drawing.Size(252, 34);
            this.lblSoftwareDeployment.TabIndex = 1;
            this.lblSoftwareDeployment.Text = "Software Deployment";
            // 
            // lollipopLabel2
            // 
            this.lollipopLabel2.AutoSize = true;
            this.lollipopLabel2.BackColor = System.Drawing.Color.Transparent;
            this.lollipopLabel2.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lollipopLabel2.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(153)))), ((int)(((byte)(153)))), ((int)(((byte)(153)))));
            this.lollipopLabel2.Location = new System.Drawing.Point(40, 470);
            this.lollipopLabel2.Name = "lollipopLabel2";
            this.lollipopLabel2.Size = new System.Drawing.Size(242, 15);
            this.lollipopLabel2.TabIndex = 2;
            this.lollipopLabel2.Text = "Select the software you would like installed:";
            this.lollipopLabel2.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.BackgroundImage = global::GFSSetup.Properties.Resources.b8;
            this.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.ClientSize = new System.Drawing.Size(329, 765);
            this.Controls.Add(this.panel2);
            this.Controls.Add(this.lollipopLabel2);
            this.Controls.Add(this.panel1);
            this.Controls.Add(this.lblSoftwareDeployment);
            this.Controls.Add(this.lblGlobalFinishing);
            this.DoubleBuffered = true;
            this.ForeColor = System.Drawing.Color.Black;
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "Form1";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Software Deployment";
            this.TransparencyKey = System.Drawing.Color.LawnGreen;
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.Form1_FormClosing);
            this.Load += new System.EventHandler(this.Form1_Load);
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            this.panel2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        private System.Windows.Forms.Panel panel1;
        private LollipopButton lollipopButton1;
        private System.Windows.Forms.Panel panel2;
        private System.Windows.Forms.PictureBox pictureBox1;
        private System.ComponentModel.BackgroundWorker bwInstall;
        private System.Windows.Forms.Timer Progressbar;
        private LollipopProgressBar lollipopProgressBar1;
        private System.Windows.Forms.Label status;
        private System.Windows.Forms.ToolTip toolTip1;
        private LollipopLabel lollipopLabel2;
        private System.Windows.Forms.Label lblGlobalFinishing;
        private System.Windows.Forms.Label lblSoftwareDeployment;
        private System.Windows.Forms.TreeView treeView1;
    }
}

